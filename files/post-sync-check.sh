#!/bin/sh

STATEDIR="/var/lib/syncthing"
STATEFILE="${STATEDIR}/actioncheck"
ACTIONSCRIPT="/etc/syncthing/post-sync.sh"

set -e

# Make sure our state directory exists
if [ ! -d ${STATEDIR} ]; then
	mkdir ${STATEDIR}
fi

# Make sure our statefile exists
if [ ! -f ${STATEFILE} ]; then
	echo $(date "+%s") > ${STATEFILE}
fi

# Get the syncthing API key
KEY=$(grep api /etc/syncthing/config.xml | sed -e 's/.*<apikey>//' -e 's/<.*//')

# Call the syncthing API to get the last sync status
RESULT=$(curl -s -X GET -H "X-API-Key:${KEY}" http://localhost:8384/rest/stats/folder)

# Store the last check timestamp
LAST_CHECK=$(cat ${STATEFILE})

# Save the current timestamp
NOW=$(date "+%s")

# Set the last modified time of the statefile to now
echo ${NOW} > ${STATEFILE}
touch -d "@${NOW}" ${STATEFILE}

# Filter out the last sync time and get it as a UNIX timestamp
LAST_SYNC=$(echo ${RESULT} | jq -r '.[].lastFile.at')
LAST_SYNC=$(date -d "${LAST_SYNC}" "+%s")

# No sync has happened yet, do nothing
if [ $LAST_SYNC -le 0 ]; then
	exit 0
fi

if [ $LAST_SYNC -ge $LAST_CHECK ]; then
	if [ -x ${ACTIONSCRIPT} ]; then
		${ACTIONSCRIPT}
	fi
fi
