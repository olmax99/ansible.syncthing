# Ansible Role: syncthing

Ansible role which installs and configures
[syncthing](hhttps://docs.syncthing.net/).

Syncthing can assist you in synchronising one or more directories across two or
more hosts. As there is a significant delay (some 60 seconds, usually), it is
not useful for time-critical applications.

The main selling point is that all communication and file transfers between the
hosts is encrypted.

A typical use case would be the distribution of certificates to a number of
hosts in a high-available setup.

## Caveats

As all hosts need to know of all other hosts, the main configuration will likely
happen in `group_vars`, rather than `host_vars`.

The names defined in `syncthing_devices` should match the hostnames exactly. If
this is not the case, `syncthing` will not know which device is the remote and
will get terribly confused.

The configuration expects an `id` as well as a certificate and private key. As
the `id` is derived from the latter, both should match. You can generate a data
set by running `syncthing -generate=/tmp/bar`, which will output the device `id`
and drop both the private and public bits in `/tmp/bar`.

## Role Variables

Available variables are listed below. There are no default values.

The definion of all devices participating in the syncthing group:

    syncthing_devices:
      - name: host0.example.com
        address: tcp://10.0.0.10:22000
        id: SLX5SAR-XBTMSYD-BT5KL2O-RAF4SQ3-TK23ZTA-RMZUPQE-ZG6SRGU-NFQNJQY
        certificate: |
          -----BEGIN CERTIFICATE-----
          MIIBmjCCASCgAwIBAgIIMa0KiFIkCFswCgYIKoZIzj0EAwIwFDESMBAGA1UEAxMJ
          c3luY3RoaW5nMB4XDTE5MDcxNjE0NTU0OVoXDTQ5MTIzMTIzNTk1OVowFDESMBAG
          A1UEAxMJc3luY3RoaW5nMHYwEAYHKoZIzj0CAQYFK4EEACIDYgAEgGh4fdCEYPbm
          aMk68cvQIqwNiKVD2ElE0A6i2xyPP/2oGelqLUYYitrgAWPL5aW/hoY/UCSAYlWl
          PAmexXzdo9bD7VRIMhkjYRqiAQFQvn+A9FZCLp+i0azGH3xdyhodoz8wPTAOBgNV
          HQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMAwGA1Ud
          EwEB/wQCMAAwCgYIKoZIzj0EAwIDaAAwZQIwDOiDj4q6zYz3Ym2e18UHZ8wcRThC
          5yQ1sDd3uEAUTdnwBbqD2I9xxDzTr6M+MV9rAjEAiMFNCjmHSgLilvjRvh0dmj9N
          Hv+K1ASlqOJKRRlR+hnvsnAF+Hg0qCFYcE6oC0M5
          -----END CERTIFICATE-----
        key: |
          -----BEGIN EC PRIVATE KEY-----
          MIGkAgEBBDD5cI9mcVflhyr9NCkrnSVNedBtwFXpPUmnaAYnEMgx3IVlQwDEfuJJ
          KuPyjlcoO1+gBwYFK4EEACKhZANiAASAaHh90IRg9uZoyTrxy9AirA2IpUPYSUTQ
          DqLbHI8//agZ6WotRhiK2uABY8vlpb+Ghj9QJIBiVaU8CZ7FfN2j1sPtVEgyGSNh
          GqIBAVC+f4D0VkIun6LRrMYffF3KGh0=
          -----END EC PRIVATE KEY-----
    
      - name: host1.example.com
        address: tcp://10.0.0.11:22000
        id: 2WR7WB2-F55IBBK-M452WWG-MVOSNTY-LLLI6U5-74WZMM3-KKBSP6A-YCAY5AK
        certificate: |
          -----BEGIN CERTIFICATE-----
          MIIBmjCCASCgAwIBAgIIZJJDRXN9EZswCgYIKoZIzj0EAwIwFDESMBAGA1UEAxMJ
          c3luY3RoaW5nMB4XDTE5MDcxNjE0NTcxOFoXDTQ5MTIzMTIzNTk1OVowFDESMBAG
          A1UEAxMJc3luY3RoaW5nMHYwEAYHKoZIzj0CAQYFK4EEACIDYgAEPQcLcJaJnNiG
          iE/K3llUN9MRCJvkLpmERtJabo1KhGPOiVLKjxPMyD45nNpPslfgNheRenctxUaY
          HC5drjB6NEPvqTYpqrpKqqF7fGU4fAFqtj0YubmVK80DWypWrS/+oz8wPTAOBgNV
          HQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMAwGA1Ud
          EwEB/wQCMAAwCgYIKoZIzj0EAwIDaAAwZQIxAKmDxkldFao/TGEQN7xnoAk5u7rP
          hUhTHmWxvYqpqaqbgLnMF3sgjTH9MuzgXpiWrgIwVQWNJ9Grc7rSugkgSLeKyZeE
          Aq1a+uuhZlGWlDj96j4xgKiIDrber+ZADqmE5Kwu
          -----END CERTIFICATE-----
        key: |
          -----BEGIN EC PRIVATE KEY-----
          MIGkAgEBBDDEpbfbx8hqds6OjWmFffMZxprruMgLHz2IMTCNSrVzwxNpQJYG80mv
          +iUwUnLsuUCgBwYFK4EEACKhZANiAAQ9Bwtwlomc2IaIT8reWVQ30xEIm+QumYRG
          0lpujUqEY86JUsqPE8zIPjmc2k+yV+A2F5F6dy3FRpgcLl2uMHo0Q++pNimqukqq
          oXt8ZTh8AWq2PRi5uZUrzQNbKlatL/4=
          -----END EC PRIVATE KEY-----

The folders to synchronise:

    syncthing_folders:
      - id: myfolder
        path: /my/synced/folder
        resync_interval: 300 # Optional

The API key to set for the REST interface:

    syncthing_api_key: ahXurashe8kieCh5zoodai7eiSaen0poo6e

## Optional functionality

This role supports the configuration of a script that will be triggered after
every file synchronisation. As this is being run through cron, there can be up
to 60 seconds of delay.

If you can drop a script in `/etc/syncthing/post-sync.sh`, it will be called
after every file synchronisation.

Optionally, you can supply the content of this script as a variable to this
role:

    syncthing_post_sync_script: |
      #!/bin/sh
      /usr/sbin/service nginx status > /dev/null 2>&1
      if [ $? -eq 0 ]; then
        /usr/sbin/service nginx reload
      fi

This will make sure nginx gets reloaded after every file synchronisation, if it
was running to begin with.

## Example Playbook

    - hosts: servers
      roles:
         - { role: tigron.syncthing }

## License

MIT

## Author Information

Created by Gerry Demaret for [Tigron bvba](http://tigron.be/).
